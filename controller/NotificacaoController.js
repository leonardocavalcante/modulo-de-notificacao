class NotificacaoController{
	constructor(element){
		this._notificacaoModel = new NotificacaoModel();
		this._notificacaoView = new NotificacaoView(element);
	}

	create(title, message, img,status){
		this._notificacaoModel.title = title;
		this._notificacaoModel.message = message;
		this._notificacaoModel.img = img;
		this._notificacaoModel.status = status;
	}

	update(){
		this._notificacaoView.update(this._notificacaoModel);
	}
}