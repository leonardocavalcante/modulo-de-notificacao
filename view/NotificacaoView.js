class NotificacaoView{
	constructor(element){
		this._element = element;
	}

	_template(model){
		return `
			<div class="notification ${model.status}">
				<div class="img">
					<img src="${model.img}"/>
				</div>
				<div class="text">
					<h3>${model.title}</h3>
					<h4>${model.message}</h4>
				</div>
				<button>X</button>
			</div>`;
	}

	update(model){
		this._element.innerHTML = this._template(model);
		setTimeout(() => {
			this._element.querySelector(".notification").classList.add("entering");
			this._element.querySelector(".notification > button").onclick = function(){
				let notificationBox = this.parentNode;
				notificationBox.classList.add("exiting");
				setTimeout(function(){
					notificationBox.parentNode.removeChild(notificationBox);
				}, 400);
			}
		}, 100);
	}
}