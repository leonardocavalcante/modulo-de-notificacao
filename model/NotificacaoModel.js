class NotificacaoModel{
	constructor(){
		this._title = "";
		this._message = "";
		this._status = "";
		this._img = "";
	}

	get title(){
		return this._title;
	}

	get message(){
		return this._message;
	}

	get status(){
		return this._status;
	}

	get img(){
		return this._img;
	}

	set title(text){
		this._title = text;
	}

	set message(text){
		this._message = text;
	}

	set status(text){
		this._status = text;
	}

	set img(path){
		this._img = path;
	}
}